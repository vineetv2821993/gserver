

import java.net.*;
import java.sql.*;
import java.awt.AWTException;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Robot;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

import javax.swing.JOptionPane;

public class MultipleSocketServer implements Runnable {
  private Socket connection;
  public static void main(String[] args) throws AWTException {

	  int port = 12125;
	  RobotExp.robot = new Robot();
	  int count = 0;
	  try{
		  ServerSocket socket1 = new ServerSocket(port);
		  System.out.println("GServer Initialized");
		  while (true) {
			  Socket connection = socket1.accept();
			  Runnable runnable = new MultipleSocketServer(connection, ++count);
			  Thread thread = new Thread(runnable);
			  thread.start();
		  }
	  }
	  catch (Exception e) {}
  }
  
  MultipleSocketServer(Socket s, int i) {
	  this.connection = s;
  }
  
  public void run() {

	  StringBuffer process = new StringBuffer();    
	  try {
	      RobotExp.sendKeyPress(Integer.parseInt(new BufferedReader(new InputStreamReader((InputStream) connection.getInputStream())).readLine()));
		  PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(connection.getOutputStream())),true);
		  out.write("0");
		  out.flush();
	  }
	  catch(Exception e){
		System.out.println("Fatal ERROR!");
	  }
	  finally {
		  try {
			  connection.close();
		  }
		  catch (IOException e){}
	  }
   
  }
}