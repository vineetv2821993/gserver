 import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

public class RobotExp {
	static boolean upKeyPress = false;
	static boolean downKeyPress = false;    
	//NAV
	final static int NAVUP = 201;
	final static int NAVDOWN = 202;
	final static int NAVLEFT = 203;
	final static int NAVRIGHT = 204;
	final static int ENTER = 205;
	final static int ESC = 206;
	final static int BACKSPACE = 207;
	final static int HOME = 208;
	final static int END = 209;
	final static int TAB= 210;
    
	private static final int UP = 65;
	private  static  final int DOWN = -67;
	private  static  final int LEFT = 66;
	private  static  final int RIGHT = 68;
	private  static  final int SWIPE_SCREEN = 100;    
	private  static  final int ZOOM_IN = 41;
	private  static  final int ZOOM_OUT = 60;
	private static final int CURSOR = 45;
	private static   final int TASKBAR = 55;
	private static final int SPACE = 27;
	
	
	private static final int Z = 26;
	private  static  final int X = 24;
	private  static  final int C = 3;
	private  static  final int V = 22;
	private  static  final int O = 15;
	private  static  final int M = 13;    
	private  static  final int E = 5;
	private  static  final int R = 17;
	private static final int H = 8;
	private static final int ALT = 28;
	private static final int NULL = 0;
	
    public static Robot robot;
    
    static final long t =300l;
    static final long s_t = 200l;
    static  final long m_t = 250l;
    
    public static void sendKeyPress(int Key) throws InterruptedException { 
		switch(Key){

			case NULL: 	System.out.print("_");
						Thread.sleep(s_t);
						if(upKeyPress ==true){
							robot.keyRelease(KeyEvent.VK_UP);
							upKeyPress =false;
						}
						Thread.sleep(s_t);
						if(downKeyPress ==true){
							robot.keyRelease(KeyEvent.VK_DOWN);
							downKeyPress = false;
						}
						break;
			case UP: 	Thread.sleep(s_t);
						if(downKeyPress ==true){
							robot.keyRelease(KeyEvent.VK_DOWN);
							downKeyPress = false;  
						}
						Thread.sleep(s_t);
						if(upKeyPress ==false){
							robot.keyPress(KeyEvent.VK_UP);
							upKeyPress = true;  
						}
						break;
			case DOWN: 	Thread.sleep(s_t);
						if(upKeyPress ==true){
							robot.keyRelease(KeyEvent.VK_UP);
							upKeyPress = false;
						}
						Thread.sleep(s_t);
						if(downKeyPress == false){
							robot.keyPress(KeyEvent.VK_DOWN);
							downKeyPress = true;
						}
						break;
			case LEFT: 	robot.keyPress(KeyEvent.VK_LEFT);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_LEFT); 
						break;
			case RIGHT: robot.keyPress(KeyEvent.VK_RIGHT);
		      			Thread.sleep(s_t);
		      			robot.keyRelease(KeyEvent.VK_RIGHT);break;
			case UP+LEFT:   
						robot.keyPress(KeyEvent.VK_LEFT);
						Thread.sleep(t);
						robot.keyRelease(KeyEvent.VK_LEFT);
						break;
			case DOWN+LEFT:        
						robot.keyPress(KeyEvent.VK_LEFT);
						Thread.sleep(t);
						robot.keyRelease(KeyEvent.VK_LEFT); 
						break;
			case UP+RIGHT:
						robot.keyPress(KeyEvent.VK_RIGHT);
						Thread.sleep(t);
						robot.keyRelease(KeyEvent.VK_RIGHT);
						break;
			case DOWN+RIGHT:
						robot.keyPress(KeyEvent.VK_RIGHT);
		      			Thread.sleep(t);
		      			robot.keyRelease(KeyEvent.VK_RIGHT); 
		      			break;
			case Z:   	robot.keyPress(KeyEvent.VK_Z);
					 	Thread.sleep(s_t);
					 	robot.keyRelease(KeyEvent.VK_Z); 
					 	break;
			case X:  	robot.keyPress(KeyEvent.VK_X);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_X); 
						break;
			case C:  	robot.keyPress(KeyEvent.VK_C);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_C); 
						break;
			case V:  	robot.keyPress(KeyEvent.VK_V);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_V); 
						break;
			case O:  	robot.keyPress(KeyEvent.VK_O);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_O); 
						break;
			case R:  	robot.keyPress(KeyEvent.VK_R);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_R); 
						break;
			case M:  	robot.keyPress(KeyEvent.VK_M);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_M); 
						break;
			case H:  	robot.keyPress(KeyEvent.VK_H);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_H); 
						break;
			case E:  	robot.keyPress(KeyEvent.VK_E);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_E); 
						break;
			case SPACE: robot.keyPress(KeyEvent.VK_SPACE);
						Thread.sleep(1000l);
						robot.keyRelease(KeyEvent.VK_SPACE);
			case ALT:  	robot.keyPress(KeyEvent.VK_ALT);
						Thread.sleep(5000l);
						robot.keyRelease(KeyEvent.VK_ALT); 
						break;        	  
			case NAVUP: robot.keyPress(KeyEvent.VK_UP);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_UP);
						break;
			case NAVDOWN:  
						robot.keyPress(KeyEvent.VK_DOWN);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_DOWN);
						break;
			case NAVLEFT:  
						robot.keyPress(KeyEvent.VK_LEFT);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_LEFT);
						break;
			case NAVRIGHT:  
						robot.keyPress(KeyEvent.VK_RIGHT);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_RIGHT);
						break;
			case ENTER: robot.keyPress(KeyEvent.VK_ENTER);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_ENTER);
						break;
			case HOME:  robot.keyPress(KeyEvent.VK_HOME);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_HOME);
						break;
			case END:  	robot.keyPress(KeyEvent.VK_END);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_END);
						break;
			case ESC:   robot.keyPress(KeyEvent.VK_ESCAPE);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_ESCAPE);
						break;
			case TAB:   robot.keyPress(KeyEvent.VK_TAB);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_TAB);
						break;
			case BACKSPACE:  
						robot.keyPress(KeyEvent.VK_BACK_SPACE);
						Thread.sleep(s_t);
						robot.keyRelease(KeyEvent.VK_BACK_SPACE);
						break;
			case SWIPE_SCREEN:  
						robot.keyPress(KeyEvent.VK_ALT);
						robot.keyPress(KeyEvent.VK_TAB);
						Thread.sleep(t);
						robot.keyRelease(KeyEvent.VK_ALT);
						robot.keyRelease(KeyEvent.VK_TAB); 
						break;
			default:	break;
		  }
       }

}	